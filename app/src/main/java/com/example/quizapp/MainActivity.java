package com.example.quizapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.example.quizapp.databinding.ActivityMainBinding;
import com.example.quizapp.model.Question;
import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    public int arrayIndex = 0;
    private Question[] questionBank = new Question[] {
            //create/instantiate question objects
            new Question(R.string.Android, false),
            new Question(R.string.Ios, true),
            new Question(R.string.React, false),
            new Question(R.string.React_N, false),
            new Question(R.string.AI, false)
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);

        binding.QuestionTextView.setText(questionBank[arrayIndex].getAnswerResId());

        binding.trueButton.setOnClickListener(v -> {
            checkAnswer(true);
        });

        binding.falseButton.setOnClickListener(v -> {
            checkAnswer(false);
        });

        binding.nextButton.setOnClickListener(v -> {
            arrayIndex = (arrayIndex + 1) % questionBank.length;
            updateQuestion();
            questionAmount();
        });

        binding.prevButton.setOnClickListener(v -> {
            if (arrayIndex > 0) {
                arrayIndex = (arrayIndex - 1) % questionBank.length;
                updateQuestion();
                questionAmount_prev();
            }
        });

        binding.buttonShake.setOnClickListener(v -> {
            flipAnimation();

        });

    }

    private void checkAnswer(boolean userChoseCorrect) {
        boolean answerIsCorrect = questionBank[arrayIndex].isAnswerTrue();
        int messageId;

        if (answerIsCorrect == userChoseCorrect){
            messageId = R.string.correct_answer;
            flipAnimation();
        }else {
            messageId = R.string.wrong_answer;
            shakeAnimation();
        }

        Snackbar.make(binding.imageView, messageId, Snackbar.LENGTH_SHORT).show();

    }

    //This is a Setter.

    private void updateQuestion() {
        binding.QuestionTextView.setText(questionBank[arrayIndex].getAnswerResId());
    }

    private void shakeAnimation() {
        Animation shake = AnimationUtils.loadAnimation(MainActivity.this,
                R.anim.shake_animation);
        binding.QuestionTextView.setAnimation(shake);
        binding.buttonShake.setText("ss");

        shake.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void questionAmount(){
        int count = arrayIndex + 1;
        binding.textViewCurrentAmount.setText(String.valueOf(count));
    }

    private void questionAmount_prev(){
        int count = arrayIndex;
        binding.textViewCurrentAmount.setText(String.valueOf(count));
    }

    private void flipAnimation() {
        Animation flip = AnimationUtils.loadAnimation(MainActivity.this,R.anim.answer_correct);
        binding.QuestionTextView.setAnimation(flip);
        binding.buttonShake.setText("ss");
    }

}